import base64
import binascii
import numpy as np
import uuid

from cog import BasePredictor, Input, Path
from io import BytesIO
from PIL import Image
from pyscrfd.face_detector import FaceDetector


class Predictor(BasePredictor):
    def setup(self) -> None:
        self.detector = FaceDetector(path='./')
        self.detector.prepare(ctx_id=0)

    @classmethod
    def as_array(cls, path: Path) -> np.ndarray:
        img = Image.open(path).convert("RGB")
        return np.asarray(img)

    @classmethod
    def encode_image(cls, image_array: np.ndarray) -> str:
        try:
            image_pil = Image.fromarray(image_array)
            buffer = BytesIO()
            image_pil.save(buffer, format="jpeg")
            b64_encoded = base64.b64encode(buffer.getvalue())
            return b64_encoded.decode('utf-8')
        except binascii.Error:
            raise Exception(f'Error when encoding image')

    def predict(
        self,
        image: Path = Input(description="Source image"),
        threshold: float = Input(default=0.5, description="Detection score threshold")
    ) -> str:
        self.detector.threshold = threshold
        as_array = self.as_array(image)
        result = self.detector.crop_face(as_array, 224)
        if result is None:
            return "null"
        return self.encode_image(result)
