import datetime
import imageio.v2 as imageio
import numpy as np

from pyscrfd.face_detector import FaceDetector


def main():
    detector = FaceDetector(path='/home/tdtimur/PycharmProjects/scrfd/')
    detector.prepare(ctx_id=0)
    img = imageio.imread('trump.jpeg')
    img_array = np.asarray(img)

    detected = detector.crop_face(img_array, 224)
    return detected


if __name__ == '__main__':
    start = datetime.datetime.utcnow()
    im = main()
    end = datetime.datetime.utcnow()
    print('Inference time: ', (end - start).total_seconds())
    imageio.imwrite('cropped.jpeg', im)
