import cv2
import numpy as np
import os

from glob import glob
from insightface.model_zoo import model_zoo
from insightface.utils import face_align
from typing import List, Optional, Tuple


class FaceDetector:
    def __init__(self, path: str):
        self.models = {}
        onnx_files = sorted(glob(os.path.join(path, "*.onnx")))
        self.threshold = 0.5

        for onnx_file in onnx_files:
            if onnx_file.find("_selfgen_") > 0:
                continue
            model = model_zoo.get_model(onnx_file)
            if model.taskname not in self.models:
                self.models[model.taskname] = model
        assert "detection" in self.models
        self.model = self.models["detection"]

    def prepare(self, ctx_id, threshold: Optional[float] = None, det_size: Optional[Tuple[int, int]] = None):
        if threshold is None or threshold < 0 or threshold > 1:
            threshold = 0.5
        if det_size is None:
            det_size = (640, 640)
        self.threshold = threshold
        assert det_size is not None
        for taskname, model in self.models.items():
            if taskname == "detection":
                model.prepare(ctx_id, input_size=det_size)
            else:
                model.prepare(ctx_id)

    def detect(
        self,
        img: np.ndarray,
        max_num: Optional[int] = None,
    ) -> Optional[List[np.ndarray]]:
        if max_num is None:
            max_num = 1
        bboxes, kpss = self.model.detect(
            img=img,
            threshold=self.threshold,
            max_num=max_num,
        )
        if bboxes.shape[0] == 0:
            return
        kps_list = []
        for i in range(bboxes.shape[0]):
            kps = None
            if kpss is not None:
                kps = kpss[i]
            kps_list.append(kps)

        return kps_list

    def crop_face(self, img: np.ndarray, size: int) -> Optional[np.ndarray]:
        kps = self.detect(img)
        if kps is None:
            return
        matrix, _ = face_align.estimate_norm(kps[0], image_size=size, mode="None")
        aligned_img = cv2.warpAffine(img, matrix, (size, size), borderValue=0.0)
        return aligned_img
